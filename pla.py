#Justin Henn
#FS-cmpsci4340-e01
#PLA

#imported packages
import matplotlib.pyplot as plt
import random
import numpy as np

#variables used for creating sets
m, b = -2, 4
lower, upper = -15, 15
random.seed(9081)

#creating linearly separable set
linear_X = [[0,0,0] for i in range(25)]
linear_d = [0]*25
for i in range(11):
    linear_X[i][0]=random.randrange(start=-1, stop=7)
    linear_X[i][1]=random.randrange(start=lower, stop=m*linear_X[i][0]+b-3)
    linear_X[i][2]=1
    linear_d[i]=-1
for i in range(11, 25):
    linear_X[i][0]=random.randrange(start=-1, stop=7)
    linear_X[i][1]=random.randrange(start=(m*linear_X[i][0]+b)+3, stop=upper)
    linear_X[i][2]=1
    linear_d[i]=1
    
#creating non-linearly separable set
non_linear_X = [[0,0,0] for i in range(25)]
for i in range(len(non_linear_X)):
    non_linear_X[i][0]=random.randrange(start=-10, stop=10)
    non_linear_X[i][1]=random.randrange(start=-10, stop=10)
    non_linear_X[i][2]=1

#creating linearly separable test set
test_set_X = [[0,0,0] for i in range(30)]
test_set_d = [0]*30
for i in range(14):
    test_set_X[i][0]=random.randrange(start=-1, stop=7)
    test_set_X[i][1]=random.randrange(start=lower, stop=m*test_set_X[i][0]+b-1)
    test_set_X[i][2]=1
    test_set_d[i]=-1
for i in range(14, 30):
    test_set_X[i][0]=random.randrange(start=-1, stop=7)
    test_set_X[i][1]=random.randrange(start=(m*test_set_X[i][0]+b)+1, stop=upper)
    test_set_X[i][2]=1
    test_set_d[i] = 1

non_linear_d = linear_d = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
                           -1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]

#graph function to graph the plot and line
def graph(X,y,w):
    for i,x in enumerate(X):
        if y[i] != 1:
            plt.scatter(X[i][0], X[i][1], s=120, marker = '_', linewidths=2)
        else:
            plt.scatter(X[i][0], X[i][1], s=120, marker = '+',linewidths=2)
    x1 = np.linspace(-10, 10)
    plt.plot(x1, (w[0]*x1/-w[1])+ (w[2]/-w[1])) 
    plt.show()
    
#function to check the test set against the line     
def check_test_set(X, y, w):
    misclassified = 0
    for i, x in enumerate(X):
        if y[i] < 0:
            if (X[i][0] * w[0]) +  (X[i][1] * w[1]) + (X[i][2] * w[2]) >= 0:
                misclassified += 1
        if y[i] > 0:
            if (X[i][0] * w[0]) +  (X[i][1] * w[1]) + (X[i][2] * w[2]) <= 0:
                misclassified += 1
    return misclassified

#function for the perceptron
def perceptron(X, y):
    w = [0]*len(X[0])
    w_v = list(w)
    old_w = list(w)
    eta = 1
    epochs = 20
    iters = 0
    e_in = -1
    vector_updates = 0
    misclassified = 0

    for t in range(epochs):
        iters += 1
        misclassified = 0
        for i, x in enumerate(X):
            if y[i] < 0:
                if (X[i][0] * w[0]) +  (X[i][1] * w[1]) + (X[i][2] * w[2]) >= 0:
                    for z in range(len(X[i])):
                        w[z] = w[z] + (eta * X[i][z]*y[i])
            if y[i] > 0:
                if (X[i][0] * w[0]) +  (X[i][1] * w[1]) + (X[i][2] * w[2]) <= 0:
                      for z in range(len(X[i])):
                        w[z] = w[z] + (eta * X[i][z]*y[i])
        for i, x in enumerate(X):
            if y[i] < 0:
                if (X[i][0] * w[0]) +  (X[i][1] * w[1]) + (X[i][2] * w[2]) >= 0:
                    misclassified += 1
            if y[i] > 0:
                if (X[i][0] * w[0]) +  (X[i][1] * w[1]) + (X[i][2] * w[2]) <= 0:
                    misclassified += 1
        if list(w) != list(old_w):
            vector_updates +=1
            old_w = list(w)
        if misclassified < e_in or e_in == -1:
            e_in = misclassified
            w_v = list(w)
            #vector_updates += 1
        if misclassified == 0:
            break
    print("Number of errors: " + str(e_in) + "/" + str(len(X)))
    print("Number of iterations: " + str(iters))
    print("Number of updates: " + str(vector_updates))
    w = list(w_v)
    return w

#main function to run the preceptron on the separate sets and print the information
linear_w = perceptron(linear_X,linear_d)
print(linear_w)
graph(linear_X, linear_d,linear_w)
non_linear_w = perceptron(non_linear_X,non_linear_d)
print(non_linear_w)
graph(non_linear_X,non_linear_d,non_linear_w)
errors = check_test_set(test_set_X, test_set_d, linear_w)
print("Errors in test set: " + str(errors) + "/" + str(len(test_set_X)))
graph(test_set_X, test_set_d, linear_w)
